const { invoke } = window.__TAURI__.tauri;

let greetInputEl;
let greetMsgEl;
var anime;
let homepageContent = ""

// loads the homepage at app startup
loadHomepage("true");
async function loadHomepage(refresh) {
  let htmlContent
  // if (homepageContent == "" || refresh == "true") {
    htmlContent = await invoke("get_homepage", {})
  //   homepageContent = htmlContent
  // } else {
  //   htmlContent = homepageContent
  // }
  document.open("text/html", "replace");
  document.write(htmlContent)
  document.close()
  // document.querySelector("body").innerHTML = await invoke("get_homepage", {})

}





async function loadAnimeInfoPage(animeLink, thumbnailLink, animeName) {
  // document.open("text/html", "replace");
  // document.write(await invoke("get_anime_info_page", {anime: anime}))
  // document.close()
  let content = document.getElementById("content")
  document.getElementById("pageTitle").innerText = animeName;
  content.innerHTML = "LOADING...";
  let thumbnailNode = document.createElement("img")
  thumbnailNode.setAttribute("src", thumbnailLink)
  thumbnailNode.setAttribute("class", "animeThumbnail")
  thumbnailNode.setAttribute("id", "animeThumbnail")

  content.appendChild(thumbnailNode);
  content.innerHTML = await invoke("get_anime_info_page", { anime: animeLink })
  //adds a delay of 0.1 second to make sure that the rest of the html page is loaded
  // setTimeout(() => {
  //   invoke("get_anime_info_page", { anime: animeLink })
  //     .then((animeInfo) => {
  //       // Update the "content" element with the anime info
  //       document.getElementById("content").innerHTML = animeInfo;
  //     })
  //     .catch((error) => {
  //       // Handle any errors that occur during the asynchronous operation
  //       console.error("Error fetching anime info:", error);
  //     });
  // }, 100); //

}



async function watchEpisode(episodeLink) {
  // document.open("text/html", "replace");
  // console.log(episodeLink)
  let episodeData =  await invoke("watch_episode", { episodeLink: episodeLink })
  // await invoke("watch_episode", { episodeLink: "https://gogoanimehd.to/one-piece-episode-1069" })
  document.getElementById("content").innerHTML = episodeData;
  // document.close()
}
