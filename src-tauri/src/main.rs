// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

mod anime_scraper;
// mod config_data;
use std;
mod html_templates;
use std::error::Error;
// use tauri::async_runtime;
// Learn more about Tauri commands at https://tauri.app/v1/guides/features/command

#[tauri::command]
fn greet(name: &str) -> String {
    format!("Hello, {}! You've been greeted from Rust!", name)
}

#[tauri::command]
fn get_homepage() -> String {
    let data = anime_scraper::get_updates();
    match data {
        Ok(v) => return html_templates::render_homepage(v).to_string(),
        Err(e) => return html_templates::return_error_page(e),
    }

    // html_templates::render_homepage(data.unwrap()).to_string()
}

// renders the page for the anime info
#[tauri::command]
fn get_anime_info_page(anime: &str) -> String {
    let data: Result<anime_scraper::AnimeDescription, Box<dyn Error>> = anime_scraper::get_anime_info(anime.to_string());
    match data {
        Ok(v) => return html_templates::render_anime_description_page(v).to_string(),
        Err(e) => return html_templates::return_error_page(e),
    }
}

//renders the page for an anime's episode
#[tauri::command]
fn watch_episode(episode_link: &str) -> String {
    println!("{}", episode_link);
    let data = anime_scraper::get_episode_data (episode_link.to_string());
    match data {
        Ok(v) => return html_templates::render_episode_page(v),
        Err(e) => return html_templates::return_error_page(e),
    }
}

fn main() {
    // anime_scraper::get_updates();
    // config_data::initialize_database();

    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![
            get_homepage,
            get_anime_info_page,
            watch_episode,
            greet
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
