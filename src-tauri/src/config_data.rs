use serde_json::{Deserializer, Serializer, Value};
use std::fs::read_dir;
use std::io::{read_to_string, Read, Write};
use tauri::api::file;

pub fn initialize_database() {
    let files = read_dir(".").unwrap();
    let mut has_config_file = false;

    for i in files {
        if (i.unwrap().path().display().to_string().as_str() == "config.json") {
            has_config_file = true;
            println!("config file created");
            break;
        } else {
            continue;
        }
    }
}

// adds an entry to the database
pub fn add_entry() {}

// updates a property of the database
pub fn update_item() {}

// deletes an entry to the database
pub fn delete_entry() {}

// returns the entry  specified
pub fn get_entry() {}

// returns all entries that are urrently airing
pub fn get_airing() {}

// checks for every anime in the database and updates its properties
// used when the database hasn't synced with the website for some time
pub fn check_for_updates() {}
